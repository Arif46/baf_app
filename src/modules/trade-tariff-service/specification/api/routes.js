// Master Country Info Routes......
const countryInfo = '/configuration/country-info/'
export const countryInfoList = countryInfo + 'list'
export const countryInfoStore = countryInfo + 'store'
export const countryInfoUpdate = countryInfo + 'update'
export const countryInfoToggle = countryInfo + 'toggle-status'

// Master application-type Routes......
const applicationType = '/configuration/application-type/'
export const applicationTypeList = applicationType + 'list'
export const applicationTypeStore = applicationType + 'store'
export const applicationTypeUpdate = applicationType + 'update'
export const applicationTypeToggle = applicationType + 'toggle-status'

// Master Capital Info Routes......
const capitalInfo = '/configuration/capital-info/'
export const capitalInfoList = capitalInfo + 'list'
export const capitalInfoStore = capitalInfo + 'store'
export const capitalInfoUpdate = capitalInfo + 'update'
export const capitalInfoToggle = capitalInfo + 'toggle-status'

// Master Port Info Routes......
const portInfo = '/configuration/port-info/'
export const portInfoList = portInfo + 'list'
export const portInfoStore = portInfo + 'store'
export const portInfoUpdate = portInfo + 'update'
export const portInfoToggle = portInfo + 'toggle-status'

// Currency Routes......
const currency = '/configuration/currency/'
export const currencyList = currency + 'list'
export const currencyStore = currency + 'store'
export const currencyUpdate = currency + 'update'
export const currencyToggle = currency + 'toggle-status'

// Product Routes......
const Product = '/configuration/product/'
export const ProductList = Product + 'list'
export const ProductStore = Product + 'store'
export const ProductUpdate = Product + 'update'
export const ProductToggle = Product + 'toggle-status'

// Unit Measurement Routes......
const unitMeasurement = '/configuration/unit-measurement/'
export const unitMeasurementList = unitMeasurement + 'list'
export const unitMeasurementStore = unitMeasurement + 'store'
export const unitMeasurementUpdate = unitMeasurement + 'update'
export const unitMeasurementToggle = unitMeasurement + 'toggle-status'

// Sectiont Routes......
const section = '/configuration/section/'
export const sectionList = section + 'list'
export const sectionStore = section + 'store'
export const sectionUpdate = section + 'update'
export const sectionToggle = section + 'toggle-status'

// Specification Routes......
const specification = '/specification/specification-info/'
export const specificationList = specification + 'list'
export const specificationStore = specification + 'store'
// Specification Verify Routes......
const specificationVerify = '/specification/specification-verify/'
export const specificationDemandWiseSearch = specificationVerify + 'verify-lists'
export const specificationVerifyStore = specificationVerify + 'store'
export const specificationVerifyList = specificationVerify + 'list'
// Specification Report Routes....
const specificationReport = '/specification/report/'
export const specificationReportApi = specificationReport + 'list'
